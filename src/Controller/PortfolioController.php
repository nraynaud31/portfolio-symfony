<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Projects;

class PortfolioController extends AbstractController
{
    #[Route('/portfolio', name: 'app_portfolio')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $projects = $doctrine->getRepository(Projects::class)->findAll();

        return $this->render('portfolio/index.html.twig', [
            'projects' => $projects,
            'controller_name' => 'Portfolio',
        ]);
    }
}
